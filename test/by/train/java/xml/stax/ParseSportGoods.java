package by.train.java.xml.stax;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

import by.train.java.xml.bean.Good;

public class ParseSportGoods {
	List<Good> gd=null;
	
	/***
	 * test basic parsing
	 */
	@Test
	public void test() {
		try {
			StaxParser p= new StaxParser("d:\\work\\JAVA\\lesson20_\\1.xml",null);
			try {
				gd=p.parse();
				for (Good gg :gd){
					System.out.println(gg);	
				}
				
			} catch (NumberFormatException | XMLStreamException e) {
				fail("parsing errors");
			}
		} catch (FileNotFoundException e) {
			fail("Xml file was not found");
		}
		assertTrue(gd.size()==2);
	}
	
	/***
	 * test with filters on description - it shouldn't read description tag
	 */
	@Test
	public void testFilter() {
		StaxParser pFltDescr=null;
		StaxParser pNorm=null;
		try {
			System.out.println("flt on descr");
			pFltDescr= new StaxParser("d:\\work\\JAVA\\lesson20_\\1.xml","description");
			pNorm= new StaxParser("d:\\work\\JAVA\\lesson20_\\1.xml",null);
			try {
				gd=pFltDescr.parse();
				for (Good gg :gd){
					System.out.println(gg);	
				}
			} catch (NumberFormatException | XMLStreamException e) {
				fail("parsing errors");
			}
		} catch (FileNotFoundException e) {
			fail("Xml file was not found");
		}
		assertTrue(gd.size()==2);
		assertTrue(gd.get(1).getDescr()==null);

		try {
			gd=pNorm.parse();
			for (Good gg :gd){
				System.out.println(gg);	
			}
		} catch (NumberFormatException | XMLStreamException e) {
			fail("parsing errors");
		}
		assertFalse(gd.get(1).getDescr()==null);
		
	}
	
}
