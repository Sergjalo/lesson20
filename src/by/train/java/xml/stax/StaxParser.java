package by.train.java.xml.stax;

import by.train.java.xml.bean.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;

public class StaxParser {
	private XMLStreamReader xmlSR;
	private List<Good> gd = new ArrayList<Good>();
	NodeNames currTag = null;
	
	/***
	 * Parser creation. Reads file and prepares stream reader. 
	 * @param flName xml file to be parsed
	 * @param fltTags comma separated list of tag names that should be skipped
	 * @throws FileNotFoundException
	 */
	public StaxParser(String flName, String fltTags) throws FileNotFoundException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			InputStream inputStream = new FileInputStream(flName);
			if ((fltTags==null)||fltTags.isEmpty()){
				xmlSR = 	xmlInputFactory.createXMLStreamReader(inputStream);
			} else {
				String[] tgs= fltTags.split(",");
				XMLStreamReader interiorX= xmlInputFactory.createXMLStreamReader(inputStream);
				// wrap to new StreamReaderDelegate with overrided method next()
				// it'll skip every mentioned in fltTags tag name.
				xmlSR = new StreamReaderDelegate(interiorX){
					public int next() throws XMLStreamException {
						boolean doit=true;
						int event=0;
			            while (doit) {
			            	  doit=false;
			                  event = super.next();
			                  if (event==XMLStreamConstants.CHARACTERS) {
			                	    for (String t:tgs){
			                	    	if (currTag==NodeNames.getName(t)) {
			                	    		doit=true;
			                	    	}
			                	    }
			                  } else {
			                	  return event;
			                  }
			            }
			            return event;
			        }					
				};
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}		
	}
	
	/***
	 * To parse xml that was set during parser creation.
	 * @return list of Goods
	 * @throws NumberFormatException
	 * @throws XMLStreamException
	 */
	public List<Good> parse () throws NumberFormatException, XMLStreamException {
		gd = null;
		Good g=null;
		
		
		while (xmlSR.hasNext()) {
			int type = xmlSR.next();
			
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				currTag = NodeNames.getName(xmlSR.getLocalName());				
				switch (currTag) {
				case GOODS:
					gd=new ArrayList<Good>();
					break;
				case GOOD:
					g = new Good();
					g.setId(Integer.parseInt(xmlSR.getAttributeValue(null, "id")));
					break;
				default:
					break;
				}
				break;

			case XMLStreamConstants.CHARACTERS:
				String txt = xmlSR.getText().trim();
				if (txt.isEmpty()) {
					break;
				}
				switch (currTag) {
				case NAME:
					g.setName(txt);
					break;
				case DESCRIPTION:
					g.setDescr(txt);
					break;
				case CUSTOMER:
					g.setCustId(Integer.parseInt(txt));
					break;
				default:
					break;
				}
				break;
				
			case XMLStreamConstants.END_ELEMENT:
				String s=xmlSR.getLocalName();
				currTag = NodeNames.getName(xmlSR.getLocalName());				
				switch (currTag) {
				case GOOD:
					gd.add(g);
					break;
				default:
					break;
				}
			}			
		}
		
		return gd;
	}

}
